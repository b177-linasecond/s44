// Get post data
fetch('https://jsonplaceholder.typicode.com/posts').then(response => response.json()).then(data => showPosts(data))

// Add post data
document.getElementById('form-add-post').addEventListener('submit', e => {
	e.preventDefault()

	fetch('https://jsonplaceholder.typicode.com/posts', {
		method : 'POST',
		body : JSON.stringify({
			title : document.getElementById('txt-title').value,
			body : document.getElementById('txt-body').value,
			userId : 1
		}),
		headers : {'Content-type' : 'application/json; charset=UTF-8'}
	})
	.then(response => response.json())
	.then(data => {
		console.log(data)

		alert(`Successfully added.`)

		document.getElementById('txt-title').value = null
		document.getElementById('txt-body').value = null
		
	})
})

// Show posts
showPosts = posts => {
	let postEntries = ''

	posts.forEach(post => {
		postEntries += `
			<div id="post-${post.id}">

				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
				
			</div>
		`
	})

	document.getElementById('div-post-entries').innerHTML = postEntries
}

// Edit post
editPost = id => {
	let title = document.getElementById(`post-title-${id}`).innerHTML
		body = document.getElementById(`post-body-${id}`).innerHTML

	document.getElementById(`txt-edit-id`).value = id
	document.getElementById(`txt-edit-title`).value = title
	document.getElementById(`txt-edit-body`).value = body
	document.getElementById('btn-submit-update').removeAttribute('disabled')
}

// Update post
document.getElementById(`form-edit-post`).addEventListener('submit', e => {
	e.preventDefault()

	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method : 'PUT',
		body : JSON.stringify({
			id : document.getElementById('txt-edit-body').value,
			title : document.getElementById('txt-edit-title').value,
			body : document.getElementById('txt-edit-body').value,
			userId : 1
		}),
		headers : {'Content-type' : 'application/json; charset=UTF-8'}
	})
	.then(response => response.json())
	.then(data => {
		console.log(data)

		alert(`Successfully updated.`)

		document.getElementById('txt-edit-body').value = null
		document.getElementById('txt-edit-title').value = null
		document.getElementById('txt-edit-body').value = null
		document.getElementById('btn-submit-update').setAttribute('disabled', true)
	})
})

deletePost = id => {
	fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
		method : 'DELETE'
	})
	.then(response => {
		console.log(response)

		alert(`Successfully deleted.`)
    });

    document.getElementById(`post-${id}`).remove();
}